# Mike

Full fledged moderation bot written in Javascript with a backend in Go.
- [bot source](https://github.com/masmeert/mikebot/)
- [api source](https://github.com/masmeert/mikeapi/)

## Invite the bot

## Run on your own machine

## Get help
[![discord invite](https://img.shields.io/discord/903629491425935431?logo=discord&style=for-the-badge)](https://discord.gg/)
